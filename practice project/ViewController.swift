//
//  ViewController.swift
//  practice project
//
//  Created by Ranjith on 29/09/2021.
//
   /*
     //Protocal - Protocol is a very powerful feature of the Swift programming language.
    it is an interface .Protocols are used to define a “blueprint of methods, propertie.
    
    
     //Data source - Datasource methods are used to generate tableView cells,header and footer before they are displaying..
     //Delegate - it is an Design pattern which has some predefined methods and properties
     Types of Unwrapping -
     7 ways to unwrap in swift
     1,Forced unwrapping — unsafe.
     2,Implicitly unwrapped variable declaration — unsafe in many cases.
     3,Optional binding — safe.
     4,Optional chaining — safe.
     5,Nil coalescing operator — safe.
     6,Guard statement — safe.
     7,Optional pattern — safe.
    
    
    //Design pattern - Design patterns are reusable solutions to common problems in software design.
    
    //MVC - The Model-View-Controller (MVC) design pattern assigns objects in an application one of three roles: model, view, or controller.
    
    //Mvvm - Model-View-ViewModel (MVVM) is a client-side design pattern. It guides the structure and design of your code to help you achieve “Separation of Concerns.”
    
    //Singleton - Singleton is a creational design pattern,
    Singleton classes are used for logging, driver objects, caching and thread pool, database connections.
    
     // pickerView-Use a PickerView widget to enable a user to select a single combination of values from multiple sets of values.
    
    DataSource method for pickerview - 1)numberOfComponentsInPickerView,2)numberOfRowsInComponent
    delegate method for pickerview -
    1)widthForComponent
    2)rowHeightForComponent
    3)titleForRow
    4)attributedTitleForRow
    5)viewForRow
    6)didSelectRow
    
    //TableView
    delegate method for Tableview -
   1) tableView:cellForRowAtIndexPath:
   2) tableView:numberOfRowsInSection:
    delegate method for Tableview -
    1)
    2)dequeueReusableCell
    
    
//ViewControllerLifeCycle
    loadView
    viewDidLoad:
    viewWillAppear:
    viewWillLayoutSubviews:
    viewDidLayoutSubviews:
    viewDidAppear:
    viewWillDisappear:
    viewDidDisappear:
    deinit:
    didReceiveMemoryWarning()
    viewWillTransition(to:with:)
    
    //ios Application LifeCycle
    Not Running
    In Active
    Active
    Background
    Suspended states.
    
    //PersistentContainer is used in CoreData. A container that encapsulates the Core Data stack in your app.
    
    //Networking -On iOS, we can use the URL Loading System to configure and make HTTP requests. This is called networking.
    //Cocoapods-
    CocoaPods manages library dependencies for your Xcode projects.
    //App Store Submission
    //CoreData-Core Data is one of the most popular frameworks provided by Apple for iOS and macOS apps.Core Data is an object graph and persistence framework.we can use coredata by creating Attribute variable on Entity
    */
   

