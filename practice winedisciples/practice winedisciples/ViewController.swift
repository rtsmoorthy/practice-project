//
//  ViewController.swift
//  practice winedisciples
//
//  Created by Ranjith on 04/10/2021.
//

import UIKit

class OrderItemDetailsData: NSObject {
    
    var quantity : String! = ""
    var bottleType : String! = ""
    var unitPrice : String! = ""
    var productName : String! = ""
    var size : String! = ""
    var productId : String! = ""
    var productDesc : String! = ""
    var totalPrice : String! = ""
    var countryName : String! = ""
    var regionName : String! = ""
    var varitalName : String! = ""
    var productSeoUrl : String! = ""
    var isStorePickup : String! = ""
    var rating : String! = ""
   
    var totalRating : String! = ""
    var productImage : String! = ""
    var productDisplayName : String! = ""
    var itemStatus : String! = ""
    var product : WineObject!
    var vintage : String! = ""
    var isAddedToCart : Int! = 0
    var isAddedToWish : Int! = 0
    var bottleSize : String! = ""
    
    func saveWithDictionary(_ aDictionary : NSDictionary!) -> OrderItemDetailsData? {
        if (aDictionary != nil && aDictionary.count > 0) {
            quantity = aDictionary["quantity"] as! String
            bottleType = aDictionary["bottleType"] as! String
            unitPrice = aDictionary["unitPrice"] as! String
            productName = aDictionary["productName"] as! String
            size = aDictionary["size"] as! String
            productId = aDictionary["productId"] as! String
            productDesc = aDictionary["productDesc"] as! String
            totalPrice = aDictionary["totalPrice"] as! String

            countryName = aDictionary["countryName"] as! String
            regionName = aDictionary["regionName"] as! String
            varitalName = aDictionary["varitalName"] as! String
            productSeoUrl = aDictionary["productSeoUrl"] as! String
            
            
            if(aDictionary["rating"] != nil) {
                if ((aDictionary["rating"]! as AnyObject).isKind(of: NSString.self)) {
                    rating = aDictionary["rating"] as! String
                } else {
                    rating = String((aDictionary["rating"] as! Int))
                }
            } else {
                rating = ""
            }
            
            if(aDictionary["totalRating"] != nil) {
                if ((aDictionary["totalRating"]! as AnyObject).isKind(of: NSString.self)) {
                    totalRating = aDictionary["totalRating"] as! String
                } else {
                    totalRating = String((aDictionary["totalRating"] as! Int))
                }

            } else {
                totalRating = ""
            }
            
            productImage = aDictionary["productImage"] as! String
            productDisplayName = aDictionary["productDisplayName"] as! String
            itemStatus = aDictionary["itemStatus"] as! String
            
            if ((aDictionary["product"] as AnyObject).isKind(of: NSString.self) == true) {
                return nil
            }
            product = WineObject().initWithDictionary(aDictionary["product"] as! NSDictionary)

        }
        return self
    }
    
}

