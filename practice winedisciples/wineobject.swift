//
//  wineobject.swift
//  practice winedisciples
//
//  Created by Ranjith on 04/10/2021.
//

import UIKit

class WineObject: NSObject {
    
    var productName : NSString! = ""
    var producer : NSString! = ""
    var vintage : NSString! = ""
    var offerDesc : NSString! = ""
    var productCategory : NSString! = ""
    var color : NSString! = ""
    var price : NSString! = ""
    var imageUrl : NSArray! = []
    var region : NSString! = ""
    var vineyard : NSString! = ""
    var product_desc : NSString! = ""
    var product_type : NSString! = ""
    var closure_type : NSString! = ""
    var country : NSString! = ""
    var rating : NSString! = ""
    var varital : NSString! = ""
    var product_id : NSString! = ""
    var productCode : NSString! = ""
    var reviewsArray : NSMutableArray! = []
    var foodsForWineString : NSString! = ""
    var maturity : NSString! = ""
    var productImagepath : NSString! = ""
    var bottleSize : NSString! = ""
    var productImage : NSString! = ""
    var isAddedToCart : Int! = 0
    var isAddedToWish : Int! = 0
    var isRated : Int! = 0
    var seoUrl : NSString! = ""
    var caseBottleCount : NSString! = "0"
    var discountValue : NSString! = ""
    var stock : NSString! = "0"
    var createdAt : Date! = Date()
    
    func initWithDictionary(_ aDictionary : NSDictionary!) -> WineObject {
        if (aDictionary != nil && aDictionary.count > 0) {
            
            if let _stock = aDictionary["stock"] {
                switch _stock {
                case is String:
                    self.setstock(_stock as! NSString)
                default:
                    self.setstock("\(_stock)" as! NSString)
                }
            }

            if let _createdAt = aDictionary["createdAt"] {
                
                func createdDateFormatted(_ date : String) {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    
                    let date : Date = dateFormatter.date(from: _createdAt as! String)!
                    print(date)
                    
                    self.createdAt = date
                }
                
                switch _createdAt {
                case is String:
                    createdDateFormatted(_createdAt as! String)
                    
                default:
                    createdDateFormatted(String(describing: _createdAt))
                }
            }

            if (aDictionary["discountPrice"] != nil){
                self.setdiscount(aDictionary["discountPrice"]  as? NSString)
            }
            
            if (aDictionary["caseBottleCount"] != nil) {
                print(aDictionary["caseBottleCount"])
                
                if let caseBottleCount = aDictionary["caseBottleCount"] {
                    
                    switch caseBottleCount {
                    case is String:
                        self.setcaseBottleCount((caseBottleCount as! String as NSString?))
                        
                    case is NSNumber:
                        self.setcaseBottleCount("\(caseBottleCount)" as! NSString)
                        
                    default:
                        break
                    }
                }
            }
            
            if (aDictionary["product_seo_url"] != nil) {
                self.setproductSeoUrl(aDictionary["product_seo_url"] as? NSString)
            }
            else if (aDictionary["seoUrl"]) != nil {
                self.setproductSeoUrl(aDictionary["seoUrl"] as? NSString)

            }
            
            if aDictionary["isReviewed"] != nil {
                if (aDictionary["isReviewed"] as AnyObject).isKind(of: NSString.self) == true {
                    isRated = (aDictionary["isReviewed"] as AnyObject).intValue
                }
                else {
                    isRated = aDictionary["isReviewed"] as! Int
                }
            }
            else {
                if let product_det = aDictionary["product_det"] as? AnyObject {
                    if let isReviewed =  product_det["isReviewed"] as? NSString {
                        isRated = Int(isReviewed.intValue)
                    }
                    else {
                        isRated = product_det["isReviewed"] as? Int ?? 0
                    }
                }
            }
            
            if (aDictionary["product_name"] != nil) {
                self.setproductName(aDictionary["product_name"] as? NSString)
            }
            else if (aDictionary["productName"] != nil) {
                self.setproductName(aDictionary["productName"] as? NSString)
            }
            
            if (aDictionary["producer"] != nil) {
                self.setproducer(aDictionary["producer"] as? NSString)
            }
            else if ((aDictionary["producerName" != nil]) != nil) {
                self.setproducer(aDictionary["producerName"] as? NSString)
            }
            
            if (aDictionary["size"] != nil) {
                self.setbottlesize(aDictionary["size"] as? NSString)
            }
            
            
            if (aDictionary["vintage"] != nil) {
                if (aDictionary["vintage"] as AnyObject).isKind(of: NSString.self) == true {
                    self.setvintage(aDictionary["vintage"] as! NSString)
                }
                else {
                    self.setvintage(NSString(format: "%d", aDictionary["vintage"] as! Int))
                }
            }
            
            if (aDictionary["product_category"] != nil) {
                self.setproductCategory(aDictionary["product_category"] as? NSString)
            } else if (aDictionary["productCategoryName"] != nil) {
                self.setproductCategory(aDictionary["productCategoryName"] as? NSString)
            }
            
            if (aDictionary["color"] != nil) {
                self.setcolor(aDictionary["color"] as? NSString)
            }
            
            if (aDictionary["price"] != nil) {
                self.setprice(aDictionary["price"] as? NSString)
            }
            
            if (aDictionary["image"] != nil) {
                if (aDictionary["image"]! as AnyObject).isKind(of: NSArray.self) {
                    self.setimageUrl(aDictionary["image"] as? NSArray)
                }
                else {
                    self.setproductImagepath(aDictionary["image"] as? NSString)
                }
            }
            
            if (aDictionary["product_imagepath"] != nil) {
                self.setproductImagepath(aDictionary["product_imagepath"] as? NSString)
            }
            else if (aDictionary["productImage"] != nil) {
                self.setproductImagepath(NSString(format: "%@%@%@", kBaseUrl,"images/products/",aDictionary["productImage"] as! String))
            }
            
            if (aDictionary["region"] != nil) {
                self.setregion(aDictionary["region"] as? NSString)
            }
            
            if (aDictionary["vineyard"] != nil) {
                self.setvineyard(aDictionary["vineyard"] as? NSString)
            }
            else if (aDictionary["vineyardName"] != nil) {
                self.setregion(aDictionary["vineyardName"] as? NSString)
            }
            
            if (aDictionary["product_desc"] != nil) {
                self.setproduct_desc(aDictionary["product_desc"] as? NSString)
            } else if (aDictionary["productDesc"] != nil) {
                self.setproduct_desc(aDictionary["productDesc"] as? NSString)
            }
            
            if (aDictionary["product_type"] != nil) {
                self.setproduct_type(aDictionary["product_type"] as? NSString)
            } else if (aDictionary["productTypeDesc"] != nil) {
                self.setproduct_type(aDictionary["productTypeDesc"] as? NSString)
            }
            
            if (aDictionary["closureTypeName"] != nil) {
                self.setclosure_type(aDictionary["closureTypeName"] as? NSString)
            }
            
            if let country = aDictionary["country"] as? AnyObject {
                if let name = country["name"] as? NSString {
                    self.setcountry(name)
                }
            } else {
                self.setcountry(aDictionary["country"] as? NSString)
                
            }
            
            if (aDictionary["product_id"] != nil) {
                self.setproduct_id(aDictionary["product_id"] as? NSString)
            }
            else if (aDictionary["productId"] != nil) {
                self.setproduct_id(NSString(format: "%d", ((aDictionary["productId"] as AnyObject) as! Int)))
            }
            
            if (aDictionary["maturity"] != nil) {
                self.setmaturity(aDictionary["maturity"] as? NSString)
            }
            
            if (aDictionary["product_code"] != nil) {
                self.setproductCode(aDictionary["product_code"] as? NSString)
            } else if (aDictionary["productCode"] != nil) {
                self.setproductCode(aDictionary["productCode"] as? NSString)
            }
            
            
            if (aDictionary["varital"] != nil) {
                self.setvarital(aDictionary["varital"] as? NSString)
            } else if (aDictionary["varitalName"] != nil) {
                self.setvarital(aDictionary["varitalName"] as? NSString)
            }
            
            if (aDictionary["foodpairing"] != nil) {
                self.setfoodsForWineString(aDictionary["foodpairing"] as? NSString)
            } else {
                self.setfoodsForWineString("")
            }
            
            if (aDictionary["rating"] != nil) {
                
                if ((aDictionary["rating"] as AnyObject).isKind(of: NSNumber.self) == true) {
                    self.setrating("\((aDictionary["rating"] as! NSNumber).floatValue)" as NSString?)
                }
                else {
                    self.setrating("\((aDictionary["rating"] as! NSString).floatValue)" as NSString?)
                }
            }
            
            
            
            if let isAddedToWishList = aDictionary["isAddedToWish"] {
                switch isAddedToWishList {
                case is String:
                    isAddedToWish = (isAddedToWishList as AnyObject).intValue

                default:
                    isAddedToWish = isAddedToWishList as! Int
                    break
                }
            }

            if let isAddedToCartList = aDictionary["isAddedToCart"] {
                switch isAddedToCartList {
                case is String:
                    isAddedToCart = (isAddedToCartList as AnyObject).intValue
                    
                default:
                    isAddedToCart = isAddedToCartList as! Int
                    break
                }
            }

            
            
            if (aDictionary["reviews"] != nil) {
                self.setreviewsArray(aDictionary["reviews"] as? NSMutableArray)
            }
            else if (aDictionary["userRating"] != nil) {
                
                if let userRatingDict = aDictionary["userRating"] as? AnyObject {
                    
                    if let rating = userRatingDict["rating"] {
                        let rating = Int(rating as! Int)
                        self.setrating(NSString(format:"%d",rating))
                        if (userRatingDict as AnyObject).isKind(of: NSDictionary.self) == true {
                            let userReviews : NSMutableArray = []
                            for ratingDict in (userRatingDict["userRating"] as! NSArray) {
                                if let ratingDictonary = ratingDict as? AnyObject {
                                let rating : NSMutableDictionary! = NSMutableDictionary()
                                rating.setValue(ratingDictonary["rating"] ?? "", forKey: "review_rating")
                                rating.setValue(ratingDictonary["user"] ?? "", forKey: "user_name")
                                rating.setValue(ratingDictonary["user_id"] ?? "", forKey: "user_id")
                                rating.setValue(ratingDictonary["review"] ?? "", forKey: "review_text")
                                userReviews.add(rating)
                                }
                            }
                            self.setreviewsArray(userReviews as NSMutableArray)
                        }
                        else {
                            print("no rating")
                        }
                    }
                }
            }
            else if let product_det = aDictionary["product_det"] as? AnyObject {
                if let userReview = product_det["userReview"]  as? AnyObject {
                    if let userRatingDict = product_det["userRating"] as? AnyObject  {
                        if let rating = userRatingDict["rating"] {
                            let rating = Int(rating as! Int)
                            self.setrating(NSString(format:"%d",rating))
                            if let userRateDict = userRatingDict as? NSDictionary  {
                                let userReviews : NSMutableArray = []
                                for ratingDict in (userRateDict["userRating"] as! NSArray) {
                                    if let ratingDictonary = ratingDict as? AnyObject {
                                        let rating : NSMutableDictionary! = NSMutableDictionary()
                                        rating.setValue(ratingDictonary["rating"], forKey: "review_rating")
                                        rating.setValue(ratingDictonary["user"], forKey: "user_name")
                                        rating.setValue(ratingDictonary["user_id"], forKey: "user_id")
                                        rating.setValue(ratingDictonary["review"], forKey: "review_text")
                                        userReviews.add(rating)
                                    }
                                }
                                self.setreviewsArray(userReviews as NSMutableArray)
                            }
                            else {
                                print("no rating")
                            }
                        }
                    }
                }
            }
        }
        return self
    }
    
    
    func initWithBuyWinesDictionary(_ aDictionary : NSDictionary!) -> WineObject {
        if (aDictionary != nil && aDictionary.count > 0) {
            self.setproductName(aDictionary["productName"] as? NSString)
            self.setproducer(aDictionary["producerName"] as? NSString)
            self.setvintage(String(describing: (aDictionary["vintage"] as? Int)) as String as NSString?)
            //self.setofferDesc(aDictionary["offer_desc"] as! NSString)
            self.setproductCategory(aDictionary["productCategoryName"] as? NSString)
            self.setcolor(aDictionary["color"] as? NSString)
            self.setprice(aDictionary["price"] as? NSString)
            self.setproductImage(aDictionary["productImage"] as? NSString)
            
            //            Sample Image Format : images/products/thumbnail/qskmgjjbnhykz35khhjw.jpg
            
            let imgUrlStr = aDictionary["productImagePath"] as? NSString
            if (imgUrlStr != nil && imgUrlStr!.length != 0) {
                productImage = "\(kBaseUrl)\(imgUrlStr!.description)" as NSString?
            } else {
                productImage = ""
            }
            
            if aDictionary["size"] != nil {
                self.setbottleSize(aDictionary["size"] as! NSString)
            }
            
            if (((aDictionary["region"] as AnyObject).isKind(of: NSString.self)) != nil) {
                self.setregion("")
            } else {
                self.setregion((aDictionary["region"] as! NSDictionary).value(forKey: "name") as! String as NSString?)
            }
            
            let _countryDict = aDictionary["country"] as? NSDictionary
            if ((_countryDict?.isKind(of: NSDictionary.self)) != nil) {
                self.setcountry((aDictionary["country"] as? NSDictionary)!.value(forKey: "name") as? String as NSString?)
            } else {
                self.setcountry("")
            }
            
            self.setvineyard(aDictionary["vineyardName"] as? NSString)
            self.setproduct_desc(aDictionary["productDesc"] as? NSString)
            self.setproduct_type(aDictionary["productType"] as? NSString)
            self.setclosure_type(aDictionary["closureTypeName"] as? NSString)

            
            if aDictionary["productId"] != nil {
                self.setproduct_id(String((aDictionary["productId"] as! Int)) as String as NSString?)
            } else if aDictionary["product_id"] != nil {
                self.setproduct_id("\(aDictionary["product_id"]!)" as! NSString)
            }

            if aDictionary["isAddedToCart"] != nil {
                if (aDictionary["isAddedToCart"] as AnyObject).isKind(of: NSString.self) == true {
                    isAddedToCart = (aDictionary["isAddedToCart"] as AnyObject).intValue
                }
                else {
                    isAddedToCart = aDictionary["isAddedToCart"] as! Int
                }
            }
            
            
            if aDictionary["isAddedToWish"] != nil {
                if (aDictionary["isAddedToWish"] as AnyObject).isKind(of: NSString.self) == true {
                    isAddedToWish = (aDictionary["isAddedToWish"] as AnyObject).intValue
                }
                else {
                    isAddedToWish = aDictionary["isAddedToWish"] as! Int
                }
            }
            
            
            if (aDictionary["productCode"] != nil) {
                self.setproductCode(String((aDictionary["productCode"] as! Int)) as String as NSString?)
            } else {
                self.setproductCode("")
            }
            
            if (aDictionary["varitalName"] != nil) {
                self.setvarital(aDictionary["varitalName"] as? NSString)
            } else {
                self.setvarital("")
            }
            
            if (aDictionary["foodpairing"] != nil) {
                self.setfoodsForWineString(aDictionary["foodpairing"] as? NSString)
            } else {
                self.setfoodsForWineString("")
            }
            
            
            //TODO: set these review and user rating
            /*userRating =         {
             rating = 4;
             reviewedUserIds =             (
             7
             );
             star1 = 0;
             star2 = 0;
             star3 = 0;
             star4 = 1;
             star5 = 0;
             totalRating = 1;
             userRating =             (
             {
             rating = 4;
             "review_date" = "12 August 2015";
             user = Clifford;
             "user_id" = 7;
             }
             );
             };
             userReview =         (
             {
             review = Tastier;
             user = Clifford;
             }
             );*/
            
            self.setreviewsArray([["review_rating" : "3.00", "review_text" : "Best for winter season", "user_id" : "116", "user_name" : "Antony"]])
            self.setrating("3")
        }
        return self
    }
    
    
    func setcaseBottleCount(_ count : NSString?) {
        if (count != NSNull() && count != nil) {
            self.caseBottleCount = count!;
        }
        else {
            self.caseBottleCount = ""
        }
    }
    
    func setdiscount(_ discount : NSString?) {
        if (discount != NSNull() && discount != nil) {
            self.discountValue = discount!;
        }
        else {
            self.discountValue = ""
        }
    }
    
    func setstock(_ stock : NSString!) {
        if (stock != NSNull() && stock != nil) {
            self.stock = stock!;
        }
        else {
            self.stock = ""
        }
    }
    
    func setproductImage(_ image : NSString!)
    {
        if (image != NSNull() && image != nil)
        {
            self.productImage = image;
        }
    }
    
    
    func setbottleSize(_ size : NSString!)
    {
        if (size != NSNull() && size != nil)
        {
            self.bottleSize = size;
        }
    }
    
    
    func setrating(_ rating : NSString?)
    {
        if (rating != NSNull() && rating != nil)         {
            self.rating = rating;
        }
        else {
            self.rating = "0"
        }
    }
    
    func setproductImagepath(_ productImagepath : NSString?) {
        if (productImagepath != NSNull() && productImagepath != nil)
        {
            self.productImagepath = productImagepath;
        }
        else {
            self.productImagepath = ""
        }
    }
    
    func setreviewsArray(_ reviewsArray : NSMutableArray?)
    {
        if (reviewsArray != NSNull() && reviewsArray != nil)
        {
            self.reviewsArray = reviewsArray;
        }
        else {
            self.reviewsArray = []
        }
    }
    
    func setfoodsForWineString(_ foodsForWineString : NSString?)
    {
        if (foodsForWineString != NSNull() && foodsForWineString != nil)
        {
            self.foodsForWineString = foodsForWineString;
        }
        else {
            self.foodsForWineString = ""
        }
    }
    
    func setvarital(_ varital : NSString?)
    {
        if (varital != NSNull() && varital != nil)
        {
            self.varital = varital;
        }
        else {
            self.varital = ""
        }
        
    }
    
    func setmaturity(_ maturity : NSString?)
    {
        if (maturity != NSNull() && maturity != nil)
        {
            self.maturity = maturity;
        }
        else {
            self.reviewsArray = []
        }
    }
    
    
    func setproduct_id(_ product_Id : NSString!)
    {
        if (product_Id != NSNull() && product_Id != nil)
        {
            self.product_id = product_Id;
        }
        else {
            self.product_id = ""
        }
    }
    
    func setproductCode(_ productCode : NSString?) {
        
        if (productCode != NSNull() && productCode != nil)
        {
            self.productCode = productCode;
        }
        else {
            self.productCode = ""
        }
    }
    
    
    func setcountry(_ country : NSString?)
    {
        if (country != NSNull() && country != nil)
        {
            self.country = country;
        }
        else {
            self.country = ""
        }
    }
    
    func setcolor(_ color : NSString?)
    {
        if (color != NSNull() && color != nil)
        {
            self.color = color;
        }
        else {
            self.color = ""
        }
    }
    
    func setclosure_type(_ closure_type : NSString?)
    {
        if (closure_type != NSNull() && closure_type != nil)
        {
            self.closure_type = closure_type;
        }
        else {
            self.closure_type = ""
        }
    }
    func setimageUrl(_ imageUrl : NSArray?)
    {
        if (imageUrl != NSNull() && imageUrl != nil)
        {
            self.imageUrl = imageUrl;
        }
        else {
            self.imageUrl = []
        }
    }
    
    func setofferDesc(_ offerDesc : NSString!)
    {
        if (offerDesc != NSNull() && offerDesc != nil)
        {
            self.offerDesc = offerDesc;
        }
        else {
            self.offerDesc = ""
        }
    }
    
    func setprice(_ price : NSString?)
    {
        if (price != NSNull() && price != nil)
        {
            self.price = price;
        }
        else {
            self.price = ""
        }
    }
    
    func setbottlesize(_ bottlesize : NSString?)
    {
        if (bottlesize != NSNull() && bottlesize != nil)
        {
            self.bottleSize = bottlesize;
        }
        else {
            self.bottleSize = ""
        }
    }
    
    func setproducer(_ producer : NSString?)
    {
        if (producer != NSNull() && producer != nil)
        {
            self.producer = producer;
        }
        else {
            self.producer = ""
        }
    }
    
    func setproduct_desc(_ product_desc : NSString?)
    {
        if (product_desc != NSNull() && product_desc != nil)
        {
            self.product_desc = product_desc;
        }
        else {
            self.product_desc = ""
        }
    }
    
    func setproduct_type(_ product_type : NSString?)
    {
        if (product_type != NSNull() && product_type != nil)
        {
            self.product_type = product_type;
        }
        else {
            self.product_type = ""
        }
    }
    
    func setproductSeoUrl(_ seoUrl : NSString?) {
        if (seoUrl != NSNull() && seoUrl != nil) {
            self.seoUrl = seoUrl;
        }
        else {
            self.seoUrl = ""
        }
    }
    
    func setproductName(_ productName : NSString?)
    {
        if (productName != NSNull() && productName != nil)
        {
            self.productName = productName;
        }
        else {
            self.productName = ""
        }
    }
    
    func setproductCategory(_ productCategory : NSString?)
    {
        if (productCategory != NSNull() && productCategory != nil)
        {
            self.productCategory = productCategory;
        }
        else {
            self.productCategory = ""
        }
    }
    func setregion(_ region : NSString?)    {
        if (region != NSNull() && region != nil)
        {
            self.region = region;
        }
        else {
            self.region = ""
        }
    }
    
    func setvineyard(_ vineyard : NSString?) {
        if (vineyard != NSNull() && vineyard != nil)
        {
            self.vineyard = vineyard;
        }
        else {
            self.vineyard = ""
        }
    }
    
    func setvintage(_ vintage : NSString!) {
        if (vintage != NSNull() && vintage != nil)
        {
            self.vintage = vintage;
        }
        else {
            self.vintage = ""
        }
    }
}


