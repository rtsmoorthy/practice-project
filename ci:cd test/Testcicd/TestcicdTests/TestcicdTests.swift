//
//  TestcicdTests.swift
//  TestcicdTests
//
//  Created by Ranjith on 07/10/2021.
//

import XCTest
@testable import Testcicd

class cdTests: XCTestCase {
    
    
    func testcalculatePrice(){
        
    let calculator = PriceCalculator()
        
        XCTAssertNotNil(calculator)
    }
    
    func testCalculateFunc() {
        let calculator = PriceCalculator()
        XCTAssertNotNil (calculator)
        let price = calculator.calculateTicketPrice(tickets: 10)
        XCTAssertNotNil (price)
    }
    func testValidPrice_100() {
        let calculator = PriceCalculator()
        XCTAssertNotNil (calculator)
        let price = calculator.calculateTicketPrice(tickets: 10)
        XCTAssertNotNil (price)
        let expectedPrice = 1000
        XCTAssertTrue(expectedPrice == price)
    }
    func testValidPrice_100_passingFiveTickets() {
        let calculator = PriceCalculator()
        XCTAssertNotNil (calculator)
        let price = calculator.calculateTicketPrice(tickets: 5)
        XCTAssertNotNil (price)
        let expectedPrice = 500
        XCTAssertTrue(expectedPrice == price)
    }
    func testNegativeNumber() {
        let calculator = PriceCalculator()
        XCTAssertNotNil (calculator)
        let price = calculator.calculateTicketPrice(tickets: -1)
        XCTAssertNotNil (price)
        let expectedPrice = 0
        XCTAssertTrue(expectedPrice == price)
    }
    func testMaximumNumber() {
        let calculator = PriceCalculator()
        XCTAssertNotNil (calculator)
        let price = calculator.calculateTicketPrice(tickets: 101)
        XCTAssertNotNil (price)
        let expectedPrice = 0
        XCTAssertTrue(expectedPrice == price)
    }
}
